import express from 'express';
const router = express.Router();

//module
import ProductDetail from '../models/ProductDetail';


// Validator 
import { isNumeric, isEmpty} from 'validator';

// Insert
// router.post('/', async (req, res) => {
//     let { name , type, description, status } = req.body;

//     // Validate input param 
//     if(isEmpty(name)){
//         res.json({
//             result: 'Failed',
//             data: {},
//             message: `name must not be empty`
//         }) ;
//         return;
//     }

//     try {
//         let newSize = await Size.create({
//           name, 
//           type: parseInt(type),
//           description, 
//           status: parseInt(type) 
//         },{
//             fields : ["name", "type", "description", "status"]
//         }
//         );

//         if(newSize){
//             res.json({
//                 result: 'Success',
//                 data: newSize,
//                 message: `Insert a new size Success`
//             });
//         }else{
//             res.json({
//                 result: 'Failed',
//                 data: {},
//                 message: `Insert a new size failed`
//             }) ;
//         }
//     } catch (error) {
//         res.json({
//             result: 'Failed',
//             data: {},
//             message: `Insert a new size failed. ERROR: ` + error
//         }) ;
//     }
 
// });

// Update 
// router.put('/:id', async (req,res) => {
//     const {id} = req.params;
//     const { name , type, description, status} = req.body;

//     if(!isNumeric(id)){
//         res.json({
//             result: "Failed",
//             data:{},
//             message:"id must be number: "
//         });
//         return;
//     }

//     try {
//         let updateSize = await Todo.findAll({
//             attributes : ['id','name','type','description','status'],
//             where : {
//                 id
//             }
//         });
        
//         if( updateSize.length > 0){
//             updateSize.forEach(async (size)=> {
//                 await size.update({
//                     name: name ? name: size.name,
//                     type: type ? type : size.type,
//                     description: description ? description : size.description,
//                     status: status ? status :size.status
//                 });
//             });
//             res.json({
//                 result: "success",
//                 data:updateSize,
//                 message:"Update a size success"
//             });
//         }
//         else{
//             res.json({
//                 result: "Failed",
//                 data:{},
//                 message:"Can not find size update"
//             });
//         }
//     } catch (error) {
//         res.json({
//             result: "Failed",
//             data:{},
//             message:"Can not find size update. ERROR: "+ error
//         });
//     }

// });

// Delete
// router.delete('/:id', async (req,res) => {
//     const {id} = req.params;
    
//     if(!isNumeric(id)){
//         res.json({
//             result: "Failed",
//             data:{},
//             message:"id must be number: "
//         });
//         return;
//     }


//     try {
//         await Task.destroy({
//             where: {
//                 todoid: id
//             }
//         });

//         let numberOfDeleteRows = await Todo.destroy({
//             where: {
//                 id
//             }
//         });
//         res.json({
//             result: "success",
//             count:numberOfDeleteRows,
//             message:"Delete a todo success"
//         });

//     } catch (error) {
//         res.json({
//             result: "Failed",
//             data:{},
//             message:"Can not to do delete. ERROR: "+ error
//         });
//     }
// });

// Query all data from db
router.get('/api/', async (req,res) => {
    try {
        let productDetails = await ProductDetail.findAll({
            attributes: ['id', 'productid', 'sizeid', 'color', 'quantity','description','status'],
        });
        res.json({
            result: "success",
            data:productDetails,
            length: productDetails.length,
            message:"Query list of productDetail success"
        });
    } catch (error) {
        res.json({
            result: "Failed",
            data:{},
            length: productDetails.length,
            message:"Querry list of productDetail failed. ERROR: "+ error
        });  
    }
});

// Get by Id
router.get('/api/:id', async (req,res) => {
    const { id } = req.params;
    
    if(!isNumeric(id)){
        res.json({
            result: "Failed",
            data:{},
            message:"id must be number: "
        });
        return;
    }


    try {
        let productDetails = await ProductDetail.findAll({
            attributes: ['productid', 'sizeid', 'color', 'quantity','description','status'],
            where: {
                id: id
            }
        });

        if(productDetails.length > 0 ){
            res.json({
                result: "success",
                data:productDetails[0],
                message:"Query id success"
            });
        }
        else{
            res.json({
                result: "Failed",
                data:{},
                message:"Querry id productDetail failed. ERROR: "+ error
            }); 
        }
     
    } catch (error) {
        res.json({
            result: "Failed",
            data:{},
            message:"Querry id productDetail failed. ERROR: "+ error 
        });  
    }
});

export default router;