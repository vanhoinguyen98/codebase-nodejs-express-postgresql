import express from 'express';
const router = express.Router();

//module
import Size from '../models/Size';


// Validator 
import { isNumeric, isEmpty} from 'validator';

// Display common info
router.get('/', async (req, res, next) => {
    try {
        let sizes = await Size.findAll({
            order: [
                ['id', 'ASC'],
                ['type', 'ASC'],
                ['name', 'ASC'],
            ],
            attributes: ['id', 'name', 'type', 'description', 'status'],
        });
        res.render('admin_size', 
        { title: 'QUẢN LÍ SIZE SẢN PHẨM',
          description: '',
          sizes: sizes,

        });
    } catch (error) {
        res.json({
            result: "Failed",
            data:{},
            length: sizes.length,
            message:"Querry list of size failed. ERROR: "+ error
        });  
    }
});

// Insert
router.post('/api/', async (req, res) => {
    let {name , type , description , status } = req.body;

    // Validate input param 
    // if(isEmpty(name)){
    //     res.json({
    //         result: 'Failed',
    //         data: {},
    //         message: `name must not be empty`
    //     }) ;
    //     return;
    // }

    try {
        let newSize = await Size.create({
          name, 
          type : parseInt(type),
          description, 
          status
        },{
            fields : ["name", "type", "description", "status"]
        }
        );

        if(newSize){
            res.json(200,{
                result: 'Success',
                data: newSize,
                message: `Insert a new size Success`
            });
        }else{
            res.json(501,{
                result: 'Failed',
                data: {},
                message: `Insert a new size failed`
            }) ;
        }
    } catch (error) {
        res.json(501,{
            result: 'Failed',
            data: {},
            message: `Insert a new size failed. ERROR: ` + error
        }) ;
    }
 
});

// Update 
router.put('/api/:id', async (req,res) => {
    const {id} = req.params;
    const { name , type, description, status} = req.body;

    if(!isNumeric(id)){
        res.json({
            result: "Failed",
            data:{},
            message:"id must be number: "
        });
        return;
    }

    try {
        let updateSize = await Size.findAll({
            attributes : ['id','name','type','description','status'],
            where : {
                id
            }
        });
        
        if( updateSize.length > 0){
            updateSize.forEach(async (size)=> {
                await size.update({
                    name: name ? name: size.name,
                    type: type ? type : size.type,
                    description: description ? description : size.description,
                    status: status ? status :size.status
                });
            });
            res.json(200,{
                result: "success",  
                data:updateSize,
                message:"Update a size success"
            });
        }
        else{
            res.json(501,{
                result: "Failed",
                data:{},
                message:"Can not find size update"
            });
        }
    } catch (error) {
        res.json(501,{
            result: "Failed",
            data:{},
            message:"Can not find size update. ERROR: "+ error
        });
    }

});

// Delete
router.delete('/api/:id', async (req,res) => {
    const {id} = req.params;
    
    if(!isNumeric(id)){
        res.json({
            result: "Failed",
            data:{},
            message:"id must be number: "
        });
        return;
    }


    try {
        // await Task.destroy({
        //     where: {
        //         todoid: id
        //     }
        // });

        let numberOfDeleteRows = await Size.destroy({
            where: {
                id
            }
        });
        res.json(200,{
            result: "success",
            count:numberOfDeleteRows,
            message:"Delete a size success"
        });

    } catch (error) {
        res.json(501,{
            result: "Failed",
            data:{},
            message:"Can not to do delete. ERROR: "+ error
        });
    }
});

// Query all data from db
router.get('/api/', async (req,res) => {
    try {
        let sizes = await Size.findAll({
            order: [
                ['id', 'ASC'],
                ['type', 'ASC'],
                ['name', 'ASC'],
            ],
            attributes: ['id', 'name', 'type', 'description', 'status'],
        });
        res.json(200,{
            result: "success",
            data:sizes,
            length: sizes.length,
            message:"Query list of size success"
        });
    } catch (error) {
        res.json(501,{
            result: "Failed",
            data:{},
            length: sizes.length,
            message:"Querry list of size failed. ERROR: "+ error
        });  
    }
});

// Get by Id
router.get('/api/:id', async (req,res) => {
    const { id } = req.params;
    
    if(!isNumeric(id)){
        res.json({
            result: "Failed",
            data:{},
            message:"id must be number: "
        });
        return;
    }

    try {
        let sizes = await Size.findAll({
            attributes: ['name', 'type', 'description', 'status'],
            where: {
                id: id
            }
        });

        if(sizes.length > 0 ){
            res.json(200,{
                result: "success",
                data:sizes[0],
                message:"Query id success"
            });
        }
        else{
            res.json(501,{
                result: "Failed",
                data:{},
                message:"Querry id size failed. ERROR: "+ error
            }); 
        }
     
    } catch (error) {
        res.json(501,{
            result: "Failed",
            data:{},
            message:"Querry id size failed. ERROR: "+ error 
        });  
    }
});

export default router;