import express from 'express';
const router = express.Router();

//module
import User from '../models/User';

// Validator 
import { isEmpty, isURL, toDate, isEmail } from 'validator';


router.get('/login', function(req, res, next) {
  res.render('login', { title: 'Login' });
});

//Register user
router.post('/register', async (req, res) => {
  let { name, password, email, profileURL, gender, dayofbirth } = req.body;

  // Validate input param 
  if (isEmpty(name) || isEmpty(password) || isEmpty(gender) || !isEmail(email) || !isURL(profileURL) || toDate(dayofbirth) === null) {
    res.json({
      result: 'Failed',
      data: {},
      message: `name, password and gender  must not be empty, Email, url , dayofbirth must be in correct format`
    });
    return;
  }

  try {
    let newUser = await User.create({
      name,
      password,
      email,
      profileurl: profileURL,
      gender,
      dayofbirth
    }, {
        fields: ["name", "password", "email", "profileurl", "gender", "dayofbirth"]
      }
    );

    if (newUser) {
      res.json({
        result: 'Success',
        data: newUser,
        message: `Insert a new User Success`
      });
    } else {
      res.json({
        result: 'Failed',
        data: {},
        message: `Insert a new User failed`
      });
    }
  } catch (error) {
    res.json({
      result: 'Failed',
      data: {},
      message: `Insert a new User failed. ERROR: ` + error
    });
  }

});

// Login User 
router.post('/login', async (req, res) => {
  const { name, email } = req.body;


  if (isEmpty(name) || !isEmail(email) || isEmpty(email)) {
    res.json({
      result: "Failed",
      data: {},
      message: "email and name must not be empty, email must be correct format "
    });
    return;
  }


  try {
    let users = await User.findAll({
      attributes: ['name', 'password', 'email', 'profileurl', 'gender', 'dayofbirth'],
      where: {
        email,
        name
      }
    });

    if (users.length > 0) {
      res.json({
        result: 'Success',
        data: users,
        message: `Login User Success`
      });
    }
    else {
      res.json({
        result: "Failed",
        data: {},
        message: "Login User Failed"
      });
    }
  } catch (error) {
    res.json({
      result: "Failed",
      data: {},
      message: "ogin User Failed. ERROR: " + error
    });
  }

});

export default router;
