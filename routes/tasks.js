import express from 'express';
const router = express.Router();

//module
import Todo from '../models/Todo';
import Task from '../models/Task';

// Validator 
import { isNumeric, isEmpty, isBoolean } from 'validator';

// Insert
router.post('/', async (req, res) => {
    let { todoid, name, isfinished } = req.body;

    // Validate input param 
    if (isEmpty(name) || !isNumeric(todoid) || !isBoolean(isfinished)) {
        res.json({
            result: 'Failed',
            data: {},
            message: `todoid must be number, name must not be empty, isfinished must be boolean`
        });
        return;
    }

    try {
        let newTask = await Task.create({
            todoid,
            name,
            isfinished
        }, {
                fields: ["todoid", "name", "isfinished"]
            }
        );

        if (newTask) {
            res.json({
                result: 'Success',
                data: newTask,
                message: `Insert a new task Success`
            });
        } else {
            res.json({
                result: 'Failed',
                data: {},
                message: `Insert a new task failed`
            });
        }
    } catch (error) {
        res.json({
            result: 'Failed',
            data: {},
            message: `Insert a new task failed. ERROR: ` + error
        });
    }

});

// Update 
router.put('/:id', async (req, res) => {
    const { id } = req.params;
    const { todoid, name, isfinished } = req.body;


    if (!isNumeric(id)) {
        res.json({
            result: "Failed",
            data: {},
            message: "id must be number: "
        });
        return;
    }


    try {
        let updateTask = await Task.findAll({
            attributes: ['id', 'todoid', 'name', 'isfinished'],
            where: {
                id
            }
        });

        if (updateTask.length > 0) {
            updateTask.forEach(async (task) => {
                await task.update({
                    name: name ? name : task.name,
                    todoid: todoid ? todoid : task.todoid,
                    isfinished: isfinished ? isfinished : task.isfinished,
                });
            });
            res.json({
                result: "success",
                data: updateTask,
                message: "Update a task success"
            });
        }
        else {
            res.json({
                result: "Failed",
                data: {},
                message: "Can not find task update"
            });
        }
    } catch (error) {
        res.json({
            result: "Failed",
            data: {},
            message: "Can not find task update. ERROR: " + error
        });
    }

});

// Delete
router.delete('/:id', async (req, res) => {
    const { id } = req.params;

    if (!isNumeric(id)) {
        res.json({
            result: "Failed",
            data: {},
            message: "id must be number: "
        });
        return;
    }


    try {
        let numberOfDeleteRows = await Task.destroy({
            where: {
                id
            }
        });
        res.json({
            result: "success",
            count: numberOfDeleteRows,
            message: "Delete a todo success"
        });

    } catch (error) {
        res.json({
            result: "Failed",
            data: {},
            message: "Can not to do delete. ERROR: " + error
        });
    }
});

// Query all data from db
router.get('/', async (req, res) => {
    try {
        let tasks = await Task.findAll({
            attributes: ['id', 'name', 'todoid', 'isfinished'],
        });
        res.json({
            result: "success",
            data: tasks,
            length: tasks.length,
            message: "Query list of task success"
        });
    } catch (error) {
        res.json({
            result: "Failed",
            data: {},
            length: tasks.length,
            message: "Querry list of task failed. ERROR: " + error
        });
    }
});

// Get by Id
router.get('/:id', async (req, res) => {
    const { id } = req.params;

    if (!isNumeric(id)) {
        res.json({
            result: "Failed",
            data: {},
            message: "id must be number: "
        });
        return;
    }

    try {
        let tasks = await Task.findAll({
            attributes: ['name', 'todoid', 'isfinished'],
            where: {
                id: id
            }
        });

        if (tasks.length > 0) {
            res.json({
                result: "success",
                data: tasks[0],
                message: "Query id success"
            });
        }
        else {
            res.json({
                result: "Failed",
                data: {},
                message: "Querry id todo failed. ERROR: " + error
            });
        }

    } catch (error) {
        res.json({
            result: "Failed",
            data: {},
            message: "Querry id todo failed. ERROR: " + error
        });
    }
});

// Get by todoid
router.get('/todoid/:todoid', async (req, res) => {
    const { todoid } = req.params;

    if (!isNumeric(todoid)) {
        res.json({
            result: "Failed",
            data: {},
            message: "todoid must be number: "
        });
        return;
    }

    try {
        let tasks = await Task.findAll({
            attributes: ['id', 'name', 'todoid', 'isfinished'],
            where: {
                todoid
            }
        });

        res.json({
            result: "success",
            data: tasks,
            message: "Query id success"
        });

    } catch (error) {
        res.json({
            result: "Failed",
            data: {},
            message: "Querry id todo failed. ERROR: " + error
        });
    }
});

export default router;