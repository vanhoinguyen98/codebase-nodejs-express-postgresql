import express from 'express';
const router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('client_homepage', { title: 'Express' });
});

export default router
  