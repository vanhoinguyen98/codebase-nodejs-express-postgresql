import express from 'express';
const router = express.Router();

var today = new Date();
var date = today.getDate() +'/'+(today.getMonth()+1)+'/'+ today.getFullYear();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('admin_dashboard', 
  { 
    title: 'BẢNG ĐIỀU KHIỂN',
    description: date
  });
});

export default router
  