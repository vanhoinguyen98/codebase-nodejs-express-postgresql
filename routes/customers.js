import express from 'express';
const router = express.Router();

//module
import Customer from '../models/Customer';


// Validator 
import { isNumeric, isEmpty} from 'validator';

// Display common info
router.get('/', function(req, res, next) {
    res.render('admin_customer', 
    { title: 'QUẢN LÍ KHÁCH HÀNG',
      description: ''
    });
});


// Insert
router.post('/api/', async (req, res) => {
    let { name , dayofbirth, address, phonenumber, email } = req.body;

    // Validate input param 
    // if(isEmpty(name)){
    //     res.json({
    //         result: 'Failed',
    //         data: {},
    //         message: `name must not be empty`
    //     }) ;
    //     return;
    // }

    try {
        let newCustomer = await Customer.create({
          name, 
          dayofbirth,
          address, 
          phonenumber,
          email
        },{
            fields : ["name", "dayofbirth", "address", "phonenumber","email"]
        }
        );

        if(newCustomer){
            res.json({
                result: 'Success',
                data: newCustomer,
                message: `Insert a new customer Success`
            });
        }else{
            res.json({
                result: 'Failed',
                data: {},
                message: `Insert a new customer failed`
            }) ;
        }
    } catch (error) {
        res.json({
            result: 'Failed',
            data: {},
            message: `Insert a new customer failed. ERROR: ` + error
        }) ;
    }
 
});

// Update 
// router.put('/:id', async (req,res) => {
//     const {id} = req.params;
//     const { name , type, description, status} = req.body;

//     if(!isNumeric(id)){
//         res.json({
//             result: "Failed",
//             data:{},
//             message:"id must be number: "
//         });
//         return;
//     }

//     try {
//         let updateSize = await Todo.findAll({
//             attributes : ['id','name','type','description','status'],
//             where : {
//                 id
//             }
//         });
        
//         if( updateSize.length > 0){
//             updateSize.forEach(async (size)=> {
//                 await size.update({
//                     name: name ? name: size.name,
//                     type: type ? type : size.type,
//                     description: description ? description : size.description,
//                     status: status ? status :size.status
//                 });
//             });
//             res.json({
//                 result: "success",
//                 data:updateSize,
//                 message:"Update a size success"
//             });
//         }
//         else{
//             res.json({
//                 result: "Failed",
//                 data:{},
//                 message:"Can not find size update"
//             });
//         }
//     } catch (error) {
//         res.json({
//             result: "Failed",
//             data:{},
//             message:"Can not find size update. ERROR: "+ error
//         });
//     }

// });

// Delete
// router.delete('/:id', async (req,res) => {
//     const {id} = req.params;
    
//     if(!isNumeric(id)){
//         res.json({
//             result: "Failed",
//             data:{},
//             message:"id must be number: "
//         });
//         return;
//     }


//     try {
//         await Task.destroy({
//             where: {
//                 todoid: id
//             }
//         });

//         let numberOfDeleteRows = await Todo.destroy({
//             where: {
//                 id
//             }
//         });
//         res.json({
//             result: "success",
//             count:numberOfDeleteRows,
//             message:"Delete a todo success"
//         });

//     } catch (error) {
//         res.json({
//             result: "Failed",
//             data:{},
//             message:"Can not to do delete. ERROR: "+ error
//         });
//     }
// });

// Query all data from db
router.get('/api/', async (req,res) => {
    try {
        let customers = await Customer.findAll({
            attributes: ['id', 'name', 'dayofbirth', 'address', 'phonenumber','email'],
        });
        res.json({
            result: "success",
            data:customers,
            length: customers.length,
            message:"Query list of customer success"
        });
    } catch (error) {
        res.json({
            result: "Failed",
            data:{},
            length: customers.length,
            message:"Querry list of customer failed. ERROR: "+ error
        });  
    }
});

// Get by Id
router.get('/api/:id', async (req,res) => {
    const { id } = req.params;
    
    if(!isNumeric(id)){
        res.json({
            result: "Failed",
            data:{},
            message:"id must be number: "
        });
        return;
    }


    try {
        let customers = await Customer.findAll({
            attributes: ['name', 'dayofbirth', 'address', 'phonenumber','email'],
            where: {
                id: id
            }
        });

        if(customers.length > 0 ){
            res.json({
                result: "success",
                data:customers[0],
                message:"Query id success"
            });
        }
        else{
            res.json({
                result: "Failed",
                data:{},
                message:"Querry id customer failed. ERROR: "+ error
            }); 
        }
     
    } catch (error) {
        res.json({
            result: "Failed",
            data:{},
            message:"Querry id customer failed. ERROR: "+ error 
        });  
    }
});

export default router;