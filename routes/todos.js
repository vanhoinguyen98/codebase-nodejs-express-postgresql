import express from 'express';
const router = express.Router();

//module
import Todo from '../models/Todo';
import Task from '../models/Task';

// Validator 
import { isNumeric, isEmpty, isInt, toDate} from 'validator';

// Insert
router.post('/', async (req, res) => {
    let { name , priority, description, duedate } = req.body;

    // Validate input param 
    if(isEmpty(name) || !isInt(priority ,{ min:0, max:2}) || toDate(duedate) === null){
        res.json({
            result: 'Failed',
            data: {},
            message: `name must not be empty, priority = 0..2 duedate must be yyyy-mm-dd`
        }) ;
        return;
    }

    try {
        let newTodo = await Todo.create({
          name, 
          priority: parseInt(priority),
          description, 
          duedate  
        },{
            fields : ["name", "priority", "description", "duedate"]
        }
        );

        if(newTodo){
            res.json({
                result: 'Success',
                data: newTodo,
                message: `Insert a new todo Success`
            });
        }else{
            res.json({
                result: 'Failed',
                data: {},
                message: `Insert a new todo failed`
            }) ;
        }
    } catch (error) {
        res.json({
            result: 'Failed',
            data: {},
            message: `Insert a new todo failed. ERROR: ` + error
        }) ;
    }
 
});

// Update 
router.put('/:id', async (req,res) => {
    const {id} = req.params;
    const { name , priority, description, duedate} = req.body;

    if(!isNumeric(id)){
        res.json({
            result: "Failed",
            data:{},
            message:"id must be number: "
        });
        return;
    }

    try {
        let updateTodo = await Todo.findAll({
            attributes : ['id','name','priority','description','duedate'],
            where : {
                id
            }
        });
        
        if( updateTodo.length > 0){
            updateTodo.forEach(async (todo)=> {
                await todo.update({
                    name: name ? name: todo.name,
                    priority: priority ? priority : todo.priority,
                    description: description ? description : todo.description,
                    duedate: duedate ? duedate :todo.duedate
                });
            });
            res.json({
                result: "success",
                data:updateTodo,
                message:"Update a todo success"
            });
        }
        else{
            res.json({
                result: "Failed",
                data:{},
                message:"Can not find to do update"
            });
        }
    } catch (error) {
        res.json({
            result: "Failed",
            data:{},
            message:"Can not find to do update. ERROR: "+ error
        });
    }

});

// Delete
router.delete('/:id', async (req,res) => {
    const {id} = req.params;
    
    if(!isNumeric(id)){
        res.json({
            result: "Failed",
            data:{},
            message:"id must be number: "
        });
        return;
    }


    try {
        await Task.destroy({
            where: {
                todoid: id
            }
        });

        let numberOfDeleteRows = await Todo.destroy({
            where: {
                id
            }
        });
        res.json({
            result: "success",
            count:numberOfDeleteRows,
            message:"Delete a todo success"
        });

    } catch (error) {
        res.json({
            result: "Failed",
            data:{},
            message:"Can not to do delete. ERROR: "+ error
        });
    }
});

// Query all data from db
router.get('/', async (req,res) => {
    try {
        let todos = await Todo.findAll({
            attributes: ['id', 'name', 'priority', 'description', 'duedate'],
        });
        res.json({
            result: "success",
            data:todos,
            length: todos.length,
            message:"Query list of todo success"
        });
    } catch (error) {
        res.json({
            result: "Failed",
            data:{},
            length: todos.length,
            message:"Querry list of todo failed. ERROR: "+ error
        });  
    }
});

// Get by Id
router.get('/:id', async (req,res) => {
    const { id } = req.params;
    
    if(!isNumeric(id)){
        res.json({
            result: "Failed",
            data:{},
            message:"id must be number: "
        });
        return;
    }


    try {
        let todos = await Todo.findAll({
            attributes: ['name', 'priority', 'description', 'duedate'],
            where: {
                id: id
            }, 
            include: {
                model: Task,
                as: 'tasks',
                require: false
            }
        });

        if(todos.length > 0 ){
            res.json({
                result: "success",
                data:todos[0],
                message:"Query id success"
            });
        }
        else{
            res.json({
                result: "Failed",
                data:{},
                message:"Querry id todo failed. ERROR: "+ error
            }); 
        }
     
    } catch (error) {
        res.json({
            result: "Failed",
            data:{},
            message:"Querry id todo failed. ERROR: "+ error 
        });  
    }
});

export default router;