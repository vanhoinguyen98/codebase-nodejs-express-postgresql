import createError from 'http-errors';
import express from 'express';
// import expressLayouts  from 'express-ejs-layouts';
import path from 'path';
import cookieParser from 'cookie-parser';
import logger from 'morgan';



import homepageRouter from './routes/homepage';

import usersRouter from './routes/users';
import todosRouter from './routes/todos';
import tasksRouter from './routes/tasks';

import loginRouter from './routes/login';
import dashboardRouter from './routes/dashboard';
import purchasesRouter from './routes/purchases';
import sizesRouter from './routes/sizes';
import categoriesRouter from './routes/categories';
import customersRouter from './routes/customers';
import productsRouter from './routes/products';
import productDetailsRouter from './routes/productDetails';
import reportRouter from './routes/report';

const app = express();

// view engine setup
app.engine('ejs', require('express-ejs-extend'));
app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));


app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
// app.use(expressLayouts);


// API EXAMPLE
app.use('/users', usersRouter);
app.use('/todos', todosRouter);
app.use('/tasks', tasksRouter);

// API client
app.use('/', homepageRouter);

// API Admin
app.use('/admin', dashboardRouter);
app.use('/admin/login', loginRouter);
app.use('/admin/size', sizesRouter);
app.use('/admin/category', categoriesRouter);
app.use('/admin/customer', customersRouter);
app.use('/admin/product', productsRouter);
app.use('/admin/productdetail', productDetailsRouter);
app.use('/admin/report', reportRouter);
app.use('/admin/dashboard', dashboardRouter);
app.use('/admin/purchase', purchasesRouter);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

export default app;
