import Sequelize, { Model } from 'sequelize';
import { sequelize } from '../databases/database';
import ProductDetail from './ProductDetail';

const Product = sequelize.define('products', {
	id: {
		type: Sequelize.INTEGER,
		primaryKey: true
	},
	categoryid: {
        type: Sequelize.INTEGER
	},
	name: {
		type: Sequelize.STRING
	},
	code: {
		type: Sequelize.STRING
	},
	type: {
		type: Sequelize.STRING
	},
	price: {
		type: Sequelize.INTEGER
	},
	discount: {
		type: Sequelize.STRING
	},
	discountamount: {
		type: Sequelize.INTEGER
	},
	totalquantity: {
		type: Sequelize.INTEGER
	},
	imageurl: {
		type: Sequelize.TEXT
	},
	material: {
		type: Sequelize.STRING
	},
	description: {
		type: Sequelize.TEXT
	},
	status: {
		type: Sequelize.INTEGER
	}

}, {
	// dont add the timestamp attributes ( updatedAt, createdAt)
	timestamps : false,
}
);

Product.hasMany(ProductDetail, { foreignKey : 'productid' , sourceKey : 'id'});
ProductDetail.belongsTo(Product, {foreignKey : 'productid', targetKey: 'id'});

export default Product;