import Sequelize, { Model } from 'sequelize';
import { sequelize } from '../databases/database';

const BillDetail = sequelize.define('billdetails', {
	id: {
		type: Sequelize.INTEGER,
		primaryKey: true
	},
	productdetailid: {
        type: Sequelize.INTEGER
	},
	billid: {
        type: Sequelize.INTEGER
    },
	quantity: {
		type: Sequelize.INTEGER
	},
	price: {
		type: Sequelize.INTEGER
	},
	discountvalue: {
		type: Sequelize.TEXT
	},
	dayofpurchase: {
		type: Sequelize.DATE
	},

}, {
	// dont add the timestamp attributes ( updatedAt, createdAt)
	timestamps : false,
}
);




export default BillDetail;