import Sequelize from 'sequelize';
import { sequelize } from '../databases/database';
import Task  from './Task';

const Todo = sequelize.define('todo',{
    id:{
        type: Sequelize.INTEGER,
        primaryKey: true
    }, 
    name: {
        type: Sequelize.STRING
    },
    priority: {
        type: Sequelize.INTEGER
    },
    description: {
        type: Sequelize.TEXT
    },
    duedate: {
        type: Sequelize.DATE
    }
},{
    // dont add the timestamp attributes ( updatedAt, createdAt)
    timestamps : false,
}   
);

Todo.hasMany(Task, { foreignKey : 'todoid' , sourceKey : 'id'});
Task.belongsTo(Todo, {foreignKey : 'todoid', targetKey: 'id'});

export default Todo;