import Sequelize, { Model } from 'sequelize';
import { sequelize } from '../databases/database';
import BillDetail from './BillDetail';

const ProductDetail = sequelize.define('productdetails', {
	id: {
		type: Sequelize.INTEGER,
		primaryKey: true
	},
	productid: {
        type: Sequelize.INTEGER
	},
	sizeid: {
        type: Sequelize.INTEGER
    },
	color: {
		type: Sequelize.STRING
	},
	quantity: {
		type: Sequelize.INTEGER
	},
	description: {
		type: Sequelize.TEXT
	},
	status: {
		type: Sequelize.INTEGER
	},

}, {
	// dont add the timestamp attributes ( updatedAt, createdAt)
	timestamps : false
}
);
ProductDetail.hasMany(BillDetail, { foreignKey : 'productdetailid' , sourceKey : 'id'});
BillDetail.belongsTo(ProductDetail, {foreignKey : 'productdetailid', targetKey: 'id'});


export default ProductDetail;