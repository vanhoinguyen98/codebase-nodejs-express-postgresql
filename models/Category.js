import Sequelize, { Model } from 'sequelize';
import { sequelize } from '../databases/database';
import Product from './Product';

const Category = sequelize.define('categories',{
    id:{
        type: Sequelize.INTEGER,
        primaryKey: true
    },
    name: {
        type: Sequelize.STRING
    },
    status: {
        type: Sequelize.INTEGER
    },

},{
	// dont add the timestamp attributes ( updatedAt, createdAt)
    timestamps : false,
}   
);
Category.hasMany(Product, { foreignKey : 'categoryid' , sourceKey : 'id'});
Product.belongsTo(Category, {foreignKey : 'categoryid', targetKey: 'id'});

export default Category;