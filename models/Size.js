import Sequelize from 'sequelize';
import { sequelize } from '../databases/database';
import ProductDetail from './ProductDetail';

const Size = sequelize.define('sizes', {
	id: {
		type: Sequelize.INTEGER,
		primaryKey: true
	},
	name: {
		type: Sequelize.STRING
	},
	type: {
		type: Sequelize.STRING
	},
	description: {
		type: Sequelize.TEXT
	},
	status: {
		type: Sequelize.INTEGER
	}
}, {
	// dont add the timestamp attributes ( updatedAt, createdAt)
	timestamps: false,
}
);

Size.hasMany(ProductDetail, { foreignKey: 'sizeid', sourceKey: 'id' });
ProductDetail.belongsTo(Size, { foreignKey: 'sizeid', targetKey: 'id' });

export default Size;