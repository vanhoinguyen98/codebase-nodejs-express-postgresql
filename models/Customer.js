import Sequelize, { Model } from 'sequelize';
import { sequelize } from '../databases/database';
import Bill from './Bill';

const Customer = sequelize.define('customers', {
	id: {
		type: Sequelize.INTEGER,
		primaryKey: true
	},
	name: {
		type: Sequelize.STRING
	},
	dayofbirth: {
		type: Sequelize.DATE
	},
	address: {
		type: Sequelize.TEXT
	},
	phonenumber: {
		type: Sequelize.STRING
	},
	email: {
		type: Sequelize.STRING
	}

}, {
	 // dont add the timestamp attributes ( updatedAt, createdAt)
	 timestamps : false,
}
);

Customer.hasMany(Bill, { foreignKey : 'todoid' , sourceKey : 'id'});
Bill.belongsTo(Customer, {foreignKey : 'todoid', targetKey: 'id'});

export default Customer;