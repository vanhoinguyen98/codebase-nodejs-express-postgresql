import Sequelize, { Model } from 'sequelize';
import { sequelize } from '../databases/database';
import BillDetail from './BillDetail';

const Bill = sequelize.define('bills', {
	id: {
		type: Sequelize.INTEGER,
		primaryKey: true
	},
	customerid: {
        type: Sequelize.INTEGER
    },
	pricetotal: {
		type: Sequelize.INTEGER
	}

}, {
	// dont add the timestamp attributes ( updatedAt, createdAt)
	timestamps : false,
}
);
Bill.hasMany(BillDetail, { foreignKey : 'billid' , sourceKey : 'id'});
BillDetail.belongsTo(Bill, {foreignKey : 'billid', targetKey: 'id'});


export default Bill;