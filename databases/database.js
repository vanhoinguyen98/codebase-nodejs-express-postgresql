import Sequelize from 'sequelize';
export const sequelize = new Sequelize(
    'shopfashion', //dbname
    'postgres', //username
    'debian', // password
    {
        dialect: 'postgres', // or mysql ,...
        host: 'localhost',
        operatorAliases: false,
        pool: {
            max:5,
            min:0, 
            require: 30000,
            idle: 10000
        }
    }
);

export const Op = Sequelize.Op;

