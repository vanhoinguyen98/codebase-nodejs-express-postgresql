$(document).ready(function () {
  addAndUpdate();
  getSizeById();
  deleteSize();
});

const addAndUpdate = () => {
  $("#bodyTableSize").on("click", "#btnAddSize", function () {
    changeStyleForm("add", "Thêm mới", "btn-primary", "btn-success");
    $("#addOrUpdateSizeForm")[0].reset();
  });

  $("#saveSize").on("click", function () {
    if (!validateFormSize()) {
      return;
    }
    let data = $("#addOrUpdateSizeForm").serialize();
    let action = $("#action").val();

    if (action == "add") {
      $.ajax({
        type: 'POST',
        url: `/admin/size/api`,
        data,
        dataType: 'json',
      })
        .done((res) => {
          $("#addOrUpdateSizeForm")[0].reset();
          reloadTableSize();
          changeLabelResult("Thêm mới thành công !", "alert-danger", "alert-success", false, 2000);
        })
        .fail((jqXHR, textStatus, errorThrown) => {
          changeLabelResult("Thêm mới không thành công. Vui lòng kiểm tra lại !", "alert-success", "alert-danger", false, 2000);
        });
    }
    else {
      let id = $("#updateSizeId").val();
      $.ajax({
        type: 'PUT',
        url: `/admin/size/api/${id}`,
        data,
        dataType: 'json',
      })
        .done((res) => {
          reloadTableSize();
          changeLabelResult("Cập nhật thành công !", "alert-danger", "alert-success", true, 2000);
        })
        .fail((jqXHR, textStatus, errorThrown) => {
          changeLabelResult("Cập nhật không thành công. Vui lòng kiểm tra lại !", "alert-success", "alert-danger", false, 2000);
        });
    }
  });
}

const getSizeById = () => {
  $("#tableSize").on("click", ".btnUpdateSize", function () {
    changeStyleForm("update", "Cập nhật", "btn-success", "btn-primary");
    let id = $(this).data('sizeid');
    $("#updateSizeId").val(id);

    $.ajax({
      type: 'GET',
      url: `/admin/size/api/${id}`,
      dataType: 'json',
    })
      .done((res) => {
        $("#addOrUpdateSizeForm").find("#name").val(res.data.name);
        $("#addOrUpdateSizeForm").find("#type").val(res.data.type);
        $("#addOrUpdateSizeForm").find("#status").val(res.data.status);
        $("#addOrUpdateSizeForm").find("#description").val(res.data.description);
      })
      .fail((jqXHR, textStatus, errorThrown) => {
        console.log(errorThrown);
      });
  });
}

const deleteSize = () => {
  $("#tableSize").on("click", ".btnDeleteSize", function () {
    let id = $(this).data('sizeid');
    $("#deleteSizeId").val(id);
  });

  $("#btnConfirmDelete").on("click", function () {
    let id = $("#deleteSizeId").val();
    $.ajax({
      type: 'DELETE',
      url: `/admin/size/api/${id}`,
      dataType: 'json',
    })
      .done((res) => {
        reloadTableSize();
        changeLabelResult("Xóa thành công !", "alert-danger", "alert-success", true, 1000);
      })
      .fail((jqXHR, textStatus, errorThrown) => {
        changeLabelResult("Xóa không thành công. Vui lòng kiểm tra lại!", "alert-success", "alert-danger", true, 1000);
      });
  });
}

const reloadTableSize = () => {
  $.ajax({
    type: 'GET',
    url: `/admin/size/api`,
    dataType: 'json',
  })
    .done((res) => {
      var info = '';
      $("#tableSize tr:has(td)").remove();
      $.each(res.data, function (key, value) {
        info += '<tr>';
        info += '<td>' + value.name + '</td>';
        info += '<td >' + value.type + '</td>';
        info += '<td >' + value.description + '</td>';
        info += '<td >  <label class="badge badge-success">' + value.status + '</label> </td>';
        info += '<td> <button class="btn btn-sm btn-outline-primary mr-1 btnUpdateSize" data-sizeId="' + value.id + '" data-toggle="modal" data-target="#modal-add-update-size"> Cập nhật </button> ';
        info += '<button class="btn btn-sm btn-outline-danger btnDeleteSize" data-sizeId="' + value.id + '" data-toggle="modal" data-target="#modal-delete-size"> Xóa </button> </td> ';
        info += '</tr>';
      });
      $('#tableSize').append(info);
    })
    .fail((jqXHR, textStatus, errorThrown) => {
      console.log(errorThrown);
    });
}

const changeStyleForm = (action, title, removeClass, addClass) => {
  $("#action").val(action);
  $("#modalTitle").text(title);
  $("#saveSize").removeClass(removeClass);
  $("#saveSize").addClass(addClass);
}

const changeLabelResult = (content, removeClass, addClass, modalHide, timeOut) => {
  $(".lblResultQuerySize").text(content);
  $(".resultQuerySize").removeClass(`hidden ${removeClass}`);
  $(".resultQuerySize").addClass(addClass);
  setTimeout(() => {
    $(".resultQuerySize").addClass("hidden");
    if (modalHide) {
      $('#modal-add-update-size').modal('hide');
      $('#modal-delete-size').modal('hide');
    }
  }, timeOut);
}

const validateFormSize = () => {
  let name = $("#addOrUpdateSizeForm").find("#name").val();
  if (name === "") {
    changeLabelResult("Tên size không được bỏ trống !", "alert-success", "alert-danger", false, 2000);
    return false;
  }
  return true;
}